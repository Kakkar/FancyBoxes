#! /bin/bash

echo -e "\e[1;33m *\e[0m Installing Fancy Boxes"
cp -r fancyboxes /lib/python3/dist-packages/
echo -e "\e[1;32m Done!"
