from fancyboxes import *
import os
from time import sleep

box1 = Box(title="Bouncy", width=34
	).update_content("\n \33[1;33mHave a lot of fun!"+'\n'.join(["\33[1;37m"+i+"\33[0m" for i in \

'''
 ⠄⠄⢿⣇⠄⠄⠘⣆⢀⣼⣿⣿⣿⣿⢿⡿⣿⢻⣿⣿⣿⣿⣿⣿⣿⣟⢧⡲⣿⢷⢦⡀ 
 ⠄⠄⠈⣿⠄⠄⠄⢙⢞⢿⣿⢹⢿⣦⢏⣱⢿⠘⣿⣝⠹⢿⣿⡽⣿⣿⣏⣆⢿⣿⡞⠁ 
 ⠄⠄⠄⢻⡀⠄⠄⠈⣾⡸⡏⢸⡾⣴⣿⣿⣶⣼⣎⢵⢀⡛⣿⣷⡙⡻⢻⡴⠨⠨⠖⠃ 
 ⠄⠄⠄⠈⣧⢀⡴⠊⢹⠁⡇⠈⢣⣿⣿⣿⣿⣦⣿⣷⣜⡳⣝⢧⢃⢣⣼⢁⠘⠆⠄⠄ 
 ⠄⠄⠄⠄⢹⡇⠄⣠⠔⠚⣅⠄⢰⣶⣦⣭⣿⣿⣿⡿⠟⠿⣷⡧⠄⣘⣟⣸⠄⠄⠄⠄ 
 ⠄⠄⠄⠄⠄⢷⠎⠄⠄⠄⣼⣦⠻⣿⣿⡟⠛⠻⢿⣿⣿⣿⡾⢱⣿⡏⠸⡏⠄⠄⠄⠄ 
 ⠄⠄⠄⠄⠄⠸⡄⠄⡄⠄⣿⢧⢗⠌⠻⣇⠿⠿⣸⣿⣿⡟⡐⣿⠟⢰⣇⠇⠄⠄⠄⠄ 
 ⠄⠄⠄⠄⠄⣠⡆⠄⠃⢠⠏⣤⢀⢢⡰⣭⣛⡉⠩⠭⡅⣾⢳⡴⡀⢸⣿⡆⠄⠄⠄⠄ 
 ⠄⠄⠄⢀⣶⡟⣽⠼⢀⡕⢀⠘⠸⢮⡳⡻⡍⡷⡆⠤⠤⠭⢸⢳⣷⢸⡟⣷⠄⠄⠄⠄ 
 ⠄⠄⣴⣿⢫⢞⣵⢏⡞⠄⢸⠄⣛⣗⠩⠄⣰⣚⠒⠂⣀⡀⢸⢸⣿⣧⠇⡼⣧⠄⠄⠄ 
 ⢠⣾⢟⡴⢫⡾⣱⢟⠄⠄⢸⠄⢈⡓⡮⡦⡬⠽⡠⠄⠔⠄⢸⠈⣿⣿⡄⣷⢹⣆⠄⠄ 
 ⡿⢁⠞⢀⣿⢣⠇⣿⠄⠄⠸⢀⢳⢣⣗⣿⡇⡔⠄⠔⠄⠄⢠⠄⠹⣿⣷⡝⣧⢻⣆⠄ 
\33[0m
'''.split('\n')
])
)


box2 = Box(title="", width=3).update_content("UwU")
header = Box(title="Most Useful Program", width=70)

x=0
y=0

speed = [1, 1]

t=0
face=0
while 1:

	t+=1
	if not t%10:
		face = not face
	box2.content = "\33[1;35m" + "UwU"*face + "OwO"*(not face) + "\33[0m"


	os.system("clear")

	display_in_row(
		box2,
		header.update_content(f"Running for \33[1;34m{t/10}\33[0m seconds"),
		box2
	)

	print("\n"*y)
	box1.display(offset=x)

	x += speed[0]
	y += speed[1]

	if x>84-34 or x<0:
		speed[0] *= -1
		x += speed[0]*2

	if y>20 or y<0:
		speed[1] *= -1
		y += speed[1]*2

	sleep(.1)